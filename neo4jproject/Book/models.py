from django.db import models
from neomodel import StructuredNode,StringProperty, UniqueIdProperty, DateProperty, EmailProperty, RelationshipTo
from django_neomodel import DjangoNode 

class Book(StructuredNode):
    code = StringProperty(unique_index=True, required=True)
    name = StringProperty(index=True)

class User(StructuredNode):
    ID = UniqueIdProperty()
    fullname =StringProperty()
    username = StringProperty()
    password = StringProperty()
    email = EmailProperty(unique_index = True)

    # Relations :
    book = RelationshipTo(Book, 'pushlish')